package com.company;

public class Main {

    public static void main(String[] args) {
        // Задание 3 :Дан текст. Нужно подсчитать количество слов и предложений в тексте. Результат вывести в консоль.
        String s = "По информации Харьковского гидрометцентра, в воскресенье, 22 марта, днем будет идти мокрый снег, местами может быть метелица. На дорогах обещают гололедицу и порывы ветра до 15-20 метров в секунду.";
        System.out.println("В указанном тексте: " + s.split(" ").length + " слов");
        int chars = 0;
        for (int i = 0; i < s.length() - 1; i++) {
            if (s.charAt(i + 1) == '.' || s.charAt(i + 1) == '!' || s.charAt(i + 1) == '?' && (s.charAt(i) == '.' || s.charAt(i + 1) == '!' || s.charAt(i + 1) == '?'))
                chars++;
        }
        System.out.println("в тексте: " + chars + "предложений");
    }
}
